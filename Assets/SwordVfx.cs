using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordVfx : MonoBehaviour
{
    public GameObject slash1;
    public void Slash1(string value)
    {
        slash1.SetActive(true);
        slash1.gameObject.GetComponent<Animator>().Play(value);
    }

    public void OffVfx()
    {
        slash1.SetActive(false);
    }
}
