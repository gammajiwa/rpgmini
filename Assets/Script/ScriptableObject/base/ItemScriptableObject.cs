using UnityEngine;
[CreateAssetMenu(fileName = "New Item", menuName = "Items/Item")]
public class ItemScriptableObject : ScriptableObject
{
    public string itemName;
    public Sprite itemIcon;
    public int itemPrice;
    public ItemEnum itemID;
    public QualityEnum quality;
    public string description;
}
