using UnityEngine;
[CreateAssetMenu(fileName = "ItemTool00", menuName = "ItemsTool/ItemTool")]
public class ItemToolScriptableObject : ScriptableObject
{
    public string itemName;
    public string itemPatch;
    public Sprite itemIcon;
    public int itemPrice;
    public ItemEnum itemID;
    public ItemToolType itemToolType;
    public QualityEnum quality;
    public string description;
}
