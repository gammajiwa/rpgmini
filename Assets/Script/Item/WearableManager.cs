using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using System.Linq;

public partial class WearableManager : MonoBehaviour
{
    [Header("Patch")]
    public List<string> hair = new List<string>();
    public List<string> faceHair = new List<string>();
    public List<string> cloth = new List<string>();
    public List<string> pant = new List<string>();
    public List<string> helmet = new List<string>();
    public List<string> armor = new List<string>();
    public List<string> weapons = new List<string>();
    public List<string> back = new List<string>();


    [Header("Sprite")]
    public List<Sprite> hairSprite = new List<Sprite>();
    public List<Sprite> faceHairSprite = new List<Sprite>();
    public List<Sprite> clothSprite = new List<Sprite>();
    public List<Sprite> pantSprite = new List<Sprite>();
    public List<Sprite> helmetSprite = new List<Sprite>();
    public List<Sprite> armorSprite = new List<Sprite>();
    public List<Sprite> weaponsSprite = new List<Sprite>();
    public List<Sprite> backSprite = new List<Sprite>();

    [Header("ScriptableObject")]
    public List<ItemToolScriptableObject> hairItemScriptableObject = new List<ItemToolScriptableObject>();
    public List<ItemToolScriptableObject> faceHairItemScriptableObject = new List<ItemToolScriptableObject>();
    public List<ItemToolScriptableObject> clothItemScriptableObject = new List<ItemToolScriptableObject>();
    public List<ItemToolScriptableObject> pantItemScriptableObject = new List<ItemToolScriptableObject>();
    public List<ItemToolScriptableObject> helmetItemScriptableObject = new List<ItemToolScriptableObject>();
    public List<ItemToolScriptableObject> armorItemScriptableObject = new List<ItemToolScriptableObject>();
    public List<ItemToolScriptableObject> weaponsItemScriptableObject = new List<ItemToolScriptableObject>();
    public List<ItemToolScriptableObject> backItemScriptableObject = new List<ItemToolScriptableObject>();

    //Name
    List<string> hairName = new List<string>();
    List<string> faceHairName = new List<string>();
    List<string> clothName = new List<string>();
    List<string> pantName = new List<string>();
    List<string> helmetName = new List<string>();
    List<string> armorName = new List<string>();
    List<string> weaponsName = new List<string>();
    List<string> backName = new List<string>();

    private void Awake()
    {
        //V1
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Items/0_Hair", hair);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Items/1_FaceHair", faceHair);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Items/2_Cloth", cloth);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Items/3_Pant", pant);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Items/4_Helmet", helmet);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Items/5_Armor", armor);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Items/6_Weapons", weapons);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Items/7_Back", back);

        //V2
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Packages/Ver121/0_Hair", hair);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Packages/Ver121/1_FaceHair", faceHair);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Packages/Ver121/2_Cloth", cloth);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Packages/Ver121/4_Helmet", helmet);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Packages/Ver121/5_armor", armor);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Packages/Ver121/6_Weapons", weapons);

        //V3
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Packages/Ver300/0_Hair", hair);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Packages/Ver300/2_Cloth", cloth);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Packages/Ver300/3_Pant", pant);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Packages/Ver300/4_Helmet", helmet);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Packages/Ver300/5_Armor", armor);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Packages/Ver300/6_Weapons", weapons);
        InitWearable("Assets/Resources/SPUM/SPUM_Sprites/Packages/Ver300/7_Back", back);

        //loadSprite
        LoadSprites(hair, hairSprite);
        LoadSprites(armor, armorSprite);
        LoadSprites(faceHair, faceHairSprite);
        LoadSprites(cloth, clothSprite);
        LoadSprites(pant, pantSprite);
        LoadSprites(helmet, helmetSprite);
        LoadSprites(weapons, weaponsSprite);
        LoadSprites(back, backSprite);

        //load ItemScriptableObject

        InitWearableScriptableObject("Assets/Script/ScriptableObject/Item/ItemTool/0_Hair", hairItemScriptableObject);
        InitWearableScriptableObject("Assets/Script/ScriptableObject/Item/ItemTool/1_FaceHair", faceHairItemScriptableObject);
        InitWearableScriptableObject("Assets/Script/ScriptableObject/Item/ItemTool/2_Cloth", clothItemScriptableObject);
        InitWearableScriptableObject("Assets/Script/ScriptableObject/Item/ItemTool/3_Pant", pantItemScriptableObject);
        InitWearableScriptableObject("Assets/Script/ScriptableObject/Item/ItemTool/4_Helmet", helmetItemScriptableObject);
        InitWearableScriptableObject("Assets/Script/ScriptableObject/Item/ItemTool/5_Armor", armorItemScriptableObject);
        InitWearableScriptableObject("Assets/Script/ScriptableObject/Item/ItemTool/6_Weapons", weaponsItemScriptableObject);
        InitWearableScriptableObject("Assets/Script/ScriptableObject/Item/ItemTool/7_Back", backItemScriptableObject);

        //addimagee
        AddImagePatch(hairItemScriptableObject, hairSprite, hair);
        AddImagePatch(faceHairItemScriptableObject, faceHairSprite, faceHair);
        AddImagePatch(clothItemScriptableObject, clothSprite, cloth);
        AddImagePatch(pantItemScriptableObject, pantSprite, pant);
        AddImagePatch(helmetItemScriptableObject, helmetSprite, helmet);
        AddImagePatch(armorItemScriptableObject, armorSprite, armor);
        AddImagePatch(weaponsItemScriptableObject, weaponsSprite, weapons);
        AddImagePatch(backItemScriptableObject, backSprite, back);

    }
    private void InitWearable(string _patch, List<string> list)
    {
        string folderPath = _patch;
        string[] spritePaths = Directory.GetFiles(folderPath, "*.png", SearchOption.AllDirectories);

        foreach (string spritePath in spritePaths)
        {
            // Get only the file name (without the path)
            string spriteName = _patch + "/" + Path.GetFileNameWithoutExtension(spritePath);

            // Add the sprite name to the armor list
            list.Add(spriteName);
        }
    }
    private void InitWearableScriptableObject(string _patch, List<ItemToolScriptableObject> list)
    {
        string folderPath = _patch;
        string[] prefabPaths = Directory.GetFiles(folderPath, "*.asset", SearchOption.AllDirectories);

        // Sort the paths based on your desired order (you may need to adjust the sorting criteria)
        prefabPaths = prefabPaths.OrderBy(path => Path.GetFileNameWithoutExtension(path)).ToArray();

        foreach (string prefabPath in prefabPaths)
        {
            ItemToolScriptableObject prefab = AssetDatabase.LoadAssetAtPath(prefabPath, typeof(ItemToolScriptableObject)) as ItemToolScriptableObject;
            if (prefab != null)
            {
                list.Add(prefab);
            }
        }
    }

    private void LoadSprites(List<string> pathList, List<Sprite> spriteList)
    {
        foreach (string path in pathList)
        {
            // Hapus "Assets/Resources/" dari setiap path
            string resourcePath = path.Replace("Assets/Resources/", "");

            // Load sprite menggunakan path yang telah dimodifikasi
            Sprite sprite = Resources.Load<Sprite>(resourcePath);

            if (sprite != null)
            {
                spriteList.Add(sprite);
            }
            else
            {
                Debug.LogError("Sprite not found: " + resourcePath);
            }
        }
    }

    void AddImagePatch(List<ItemToolScriptableObject> ScriptableObject, List<Sprite> Sprite, List<string> name)
    {
        for (int i = 0; i < ScriptableObject.Count; i++)
        {
            ScriptableObject[i].itemIcon = Sprite[i];
            ScriptableObject[i].itemPatch = name[i];
        }
    }
}