using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemDropItem : MonoBehaviour
{
    public List<DropItem> tire1 = new List<DropItem>();
    public List<DropItem> tire2 = new List<DropItem>();
    public List<DropItem> tire3 = new List<DropItem>();
    public List<DropItem> tire4 = new List<DropItem>();

    public float tire1DropChance = 0.8f;
    public float tire2DropChance = 0.4f;
    public float tire3DropChance = 0.2f;
    public float tire4DropChance = 0.1f;

    private ItemScriptableObject DropItem()
    {
        float randomValue = Random.value;
        Debug.Log(randomValue);
        if (randomValue < tire1DropChance)
        {
            return tire1[Random.Range(0, tire1.Count)].item;
        }
        else if (randomValue < tire1DropChance + tire2DropChance)
        {
            return tire2[Random.Range(0, tire2.Count)].item;
        }
        else if (randomValue < tire1DropChance + tire2DropChance + tire3DropChance)
        {
            return tire3[Random.Range(0, tire3.Count)].item;
        }
        else
        {
            return tire4[Random.Range(0, tire4.Count)].item;
        }
    }


    public void GetDropItem()
    {
        GameObject drop = ObjectPoolManager.Instance.GetObjectFromPool("Dropitem", transform.position, Quaternion.identity);
        drop.GetComponent<DropItem>().item = DropItem();
    }
}
