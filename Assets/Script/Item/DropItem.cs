using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem : MonoBehaviour
{
    [SerializeField] private SpriteRenderer sprite;

    public ItemScriptableObject item;

    Transform player;
    public Rigidbody2D rb;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    private void FixedUpdate()
    {
        Vector2 Dir = (player.position - transform.position).normalized;
        float rotateAmount = Vector3.Cross(Dir, transform.up).z;
        rb.angularVelocity = -rotateAmount * 400;
        rb.velocity = transform.up * 50;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameObject obj = ObjectPoolManager.Instance.GetObjectFromPool("hitvfx", player.transform.position, Quaternion.identity);
            obj.GetComponent<Animator>().Play("drop2");
            gameObject.SetActive(false);
            InventorySystem.Instance.AddItem(item);
        }
    }
}
