using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class ItemGameObjectUI : MonoBehaviour
{
    public Action<InventoryItem> OnClikItemEvent;
    public TextMeshProUGUI amountText;
    public TextMeshProUGUI qualtyText;
    public Image image;
    InventoryItem inventoryIte;
    public string des;

    public void SetItem(InventoryItem item)
    {
        inventoryIte = item;
        image.sprite = item.item.itemIcon;
        amountText.text = item.amount.ToString();
        qualtyText.text = item.item.quality.ToString();
        des = item.item.description;
    }

    public void UpdateItem(int item)
    {
        amountText.text = item.ToString();
    }

    public void OnClikItem()
    {
        OnClikItemEvent?.Invoke(inventoryIte);
    }
}
