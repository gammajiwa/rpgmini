using System.Collections.Generic;
using UnityEngine;

public class InventorySystem : MonoBehaviour
{
    public static InventorySystem Instance;
    public int inventoryCapacity = 20;
    public List<InventoryItem> items = new List<InventoryItem>();
    private void Awake()
    {
        Instance = this;
    }
    public bool AddItem(ItemScriptableObject item, int amount = 1)
    {
        if (amount <= 0)
        {
            return false;
        }

        if (items.Count >= inventoryCapacity)
        {
            return false;
        }

        foreach (InventoryItem inventoryItem in items)
        {
            if (inventoryItem.item == item)
            {
                inventoryItem.amount += amount;
                return true;
            }
        }

        if (items.Count < inventoryCapacity)
        {
            InventoryItem newInventoryItem = new InventoryItem();
            newInventoryItem.item = item;
            newInventoryItem.amount = amount;
            items.Add(newInventoryItem);
            return true;
        }

        return false;
    }

    public void RemoveItem(ItemScriptableObject item, int amount = 1)
    {
        if (amount <= 0)
        {
            return;
        }

        foreach (InventoryItem inventoryItem in items)
        {
            if (inventoryItem.item == item)
            {
                inventoryItem.amount -= amount;
                if (inventoryItem.amount <= 0)
                {
                    items.Remove(inventoryItem);
                }
                else
                {
                    Debug.Log("Item " + item.itemName + " amount decreased by " + amount + ".");
                }
                return;
            }
        }
    }

    public void ClearInventory()
    {
        items.Clear();
        Debug.Log("Inventory cleared.");
    }
}

[System.Serializable]
public class InventoryItem
{
    public ItemScriptableObject item;
    public int amount;
}

