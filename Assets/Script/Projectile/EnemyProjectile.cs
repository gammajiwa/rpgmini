using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : BaseProjectile
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        Player player = other.gameObject.GetComponent<Player>();
        if (player != null)
        {
            // player.TakeDamage(type, Damage);
            gameObject.SetActive(false);
            player.VfxHit(4);
        }
    }
}
