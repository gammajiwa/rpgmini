using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseProjectile : MonoBehaviour
{
    public float Damage;
    public DamageEnum type;
    private Transform target;  // Referensi ke GameObject target yang akan diikuti
    private float speed = 30f;  // Kecepatan pergerakan proyektil
    public Rigidbody2D rb;

    private void FixedUpdate()
    {
        if (target != null)
        {
            // Hitung arah ke target
            Vector3 Dir = (target.position - transform.position).normalized;
            // Terapkan gaya ke Rigidbody untuk menggerakkan proyektil
            rb.velocity = Dir * speed;
        }
        else rb.velocity = Vector3.zero;
    }

    public void SetProjectile(Transform _target, float _damage, DamageEnum _type)
    {
        target = _target;
        Damage = _damage;
        type = _type;
    }
}
