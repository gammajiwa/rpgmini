using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Pool
{
    public Transform parent;
    public GameObject prefabs;
    public string tag;
    public int size;
    public List<GameObject> Counting;
}

public class ObjectPoolManager : MonoBehaviour
{
    public static ObjectPoolManager Instance;
    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> objectPool;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        objectPool = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool item in pools)
        {
            Queue<GameObject> poolObject = new Queue<GameObject>();
            for (int i = 0; i < item.size; i++)
            {
                GameObject obj = Instantiate(item.prefabs);
                obj.transform.SetParent(item.parent);
                obj.SetActive(false);
                poolObject.Enqueue(obj);
                item.Counting.Add(obj);
            }
            objectPool.Add(item.tag, poolObject);
        }
    }

    public GameObject GetObjectFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        if (!objectPool.ContainsKey(tag))
            return null;
        //     // Pool is empty, create a new object
        if (AllObjectsActiveInTag(tag))
            CreateNewObject(tag, position, rotation);

        GameObject obj = objectPool[tag].Dequeue();
        obj.SetActive(true);
        obj.transform.position = position;
        obj.transform.rotation = rotation;
        objectPool[tag].Enqueue(obj);
        return obj;
    }

    private void CreateNewObject(string tag, Vector3 position, Quaternion rotation)
    {
        foreach (Pool obj in pools)
        {
            if (obj.tag == tag)
            {
                for (int i = 0; i < obj.size; i++)
                {
                    {
                        GameObject newObj = Instantiate(obj.prefabs);
                        newObj.transform.SetParent(obj.parent);
                        newObj.SetActive(false);
                        objectPool[tag].Enqueue(newObj);
                        obj.Counting.Add(newObj);
                    }
                }
            }
        }
        GetObjectFromPool(tag, position, rotation);
    }

    private bool AllObjectsActiveInTag(string tag)
    {
        foreach (GameObject item in pools.Find(pool => pool.tag == tag).Counting)
        {
            if (!item.activeInHierarchy)
            {
                return false;
            }
        }
        return true;
    }

}
