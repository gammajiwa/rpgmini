public enum DamageEnum
{
    phscDmg,
    magicDmg,
    critDmg,

}

public enum StatAnimation
{
    idle,
    run,
    Attack_Normal,
    Attack_Bow,
    Attack_Magic,
    Debuff_Stun,
    Death,
    Skill_Bow,
    Skill_Magic,
    Skill_Normal,
    Damage
}

public enum StateCondition
{
    walk,
    atk,
    dead,
    stuned,
    patrol,

}