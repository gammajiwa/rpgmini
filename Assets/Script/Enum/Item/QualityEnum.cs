public enum QualityEnum
{
    Common,
    Uncommon,
    Rare,
    Epic,
    Legendary
}
