
[System.Serializable]
public class WeaponAtribut
{
    public ClassAtribut atribut;
    public ItemType Type;
    public WeaponType weaponType;
    public DamageEnum damageType;
}
[System.Serializable]
public class ArmorAtribut
{
    public ClassAtribut atribut;
    public ItemType Type;
}
[System.Serializable]
public class AdditionalItemAtribut
{
    public ClassAtribut atribut;
    public ItemType Type;
}

public enum WeaponType
{
    Melee,
    Range,
    Magic,
    Shield
}

public enum ItemType
{
    Weapon,
    Armor,
    Magic,
    AdditionalItem
}
