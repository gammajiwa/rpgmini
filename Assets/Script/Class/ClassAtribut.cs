using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class ClassAtribut
{
    public float HealthPoint;
    public float CurrHealthPoint;
    public int Strength;
    public int Intelligent;
    public int Dexterity;
    public int Endurance;
    public float RankAtk;
    public float SpeedAtk;
    public float MoveSpeed;
    public float distanceChase;
    public float ApproachableDistance;
    public ClassAtribut(ClassAtribut addAtribut)
    {
        this.HealthPoint += addAtribut.HealthPoint;
        this.CurrHealthPoint = HealthPoint;
        this.Strength += addAtribut.Strength;
        this.Intelligent += addAtribut.Intelligent;
        this.Dexterity += addAtribut.Dexterity;
        this.Endurance += addAtribut.Endurance;
        this.RankAtk += addAtribut.RankAtk;
        this.SpeedAtk += addAtribut.SpeedAtk;
        this.MoveSpeed += addAtribut.MoveSpeed;
        this.distanceChase += addAtribut.distanceChase;
        this.ApproachableDistance += addAtribut.ApproachableDistance;
    }

    public void AddAtribut(ClassAtribut addAtribut)
    {
        HealthPoint += addAtribut.HealthPoint;
        CurrHealthPoint = HealthPoint;
        Strength += addAtribut.Strength;
        Intelligent += addAtribut.Intelligent;
        Dexterity += addAtribut.Dexterity;
        Endurance += addAtribut.Endurance;
        RankAtk += addAtribut.RankAtk;
        SpeedAtk += addAtribut.SpeedAtk;
        MoveSpeed += addAtribut.MoveSpeed;
        distanceChase += addAtribut.distanceChase;
        ApproachableDistance += addAtribut.ApproachableDistance;
    }

    public void ResetAtribut()
    {
        HealthPoint = 0;
        Strength = 0;
        Intelligent = 0;
        Dexterity = 0;
        Endurance = 0;
        RankAtk = 0;
        SpeedAtk = 0;
        MoveSpeed = 0;
        CurrHealthPoint = 0;
        distanceChase = 0;
        ApproachableDistance = 0;
    }
}
