using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    public Transform popUpScore;
    [SerializeField]
    private Image HpPoint, ManaPoint, EnergyPoint;
    private void Awake()
    {
        Instance = this;
    }

    public void HpPlayer(float curr, float maxCurr)
    {
        HpPoint.fillAmount = curr / maxCurr;
    }
    public void ManaPlayer(float curr, float maxCurr)
    {
        ManaPoint.fillAmount = curr / maxCurr;
    }
    public void EnergyPlayer(float curr, float maxCurr)
    {
        EnergyPoint.fillAmount = curr / maxCurr;
    }


    private void SetItemToolCardList(List<ItemToolScriptableObject> cards, List<GameObject> cardListObject, Transform parentTransform)
    {
        if (cardListObject != null)
        {
            for (int i = 0; i < cardListObject.Count; i++)
            {
                cardListObject[i].SetActive(false);
            }
        }
        if (cards != null)
        {
            int childCount = parentTransform.childCount;
            int dataCount = Mathf.Min(cards.Count, childCount);
            for (int i = 0; i < dataCount; i++)
            {
                GameObject _card = parentTransform.GetChild(i).gameObject;
                // _card.GetComponent<UICard>().SetCardDisplay(cards[i].GetCard(), uiLock);
                cardListObject[i].SetActive(true);
            }
            for (int i = childCount; i < cards.Count; i++)
            {
                // GameObject _card = Instantiate(prefabsUICard);
                // _card.transform.SetParent(parentTransform, false);
                // _card.GetComponent<UICard>().SetCardDisplay(cards[i].GetCard(), uiLock);
                // _card.GetComponent<UICard>().OnCardClikEvent += HandleOnCardInfoEvent;
                // cardListObject.Add(_card);
            }
        }
    }


}
