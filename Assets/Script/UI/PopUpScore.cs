using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopUpScore : MonoBehaviour
{
    TextMeshPro textMesh;
    float timer;
    private void Awake()
    {
        textMesh = transform.GetComponent<TextMeshPro>();
    }
    public static PopUpScore AddPopUp(float score, Vector3 location)
    {
        location.x = Random.Range(location.x - 2, location.x + 2);
        // Transform popup = Instantiate(UIManager.Instance.popUpScore, location, Quaternion.identity);
        GameObject popup = ObjectPoolManager.Instance.GetObjectFromPool("Score", location, Quaternion.identity);
        PopUpScore damage = popup.GetComponent<PopUpScore>();
        damage.SetText(score);
        return damage;
    }

    public void SetText(float score)
    {
        timer = 2;
        textMesh.SetText(score.ToString());
    }

    private void Update()
    {
        timer -= 1 * Time.deltaTime;
        if (timer <= 0)
            gameObject.SetActive(false);
    }
}

