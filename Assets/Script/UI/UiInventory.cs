using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UiInventory : MonoBehaviour
{
    public InventorySystem inventorySystem;
    public GameObject prefabsItemInventory;
    public Transform perentInventory;
    public Dictionary<ItemScriptableObject, GameObject> exiesItems = new Dictionary<ItemScriptableObject, GameObject>();
    public TextMeshProUGUI desText;
    public Image desImage;

    private void OnEnable()
    {
        ShowInventory();
        initDescription();
    }

    public void ShowInventory()
    {
        if (inventorySystem.items != null)
        {
            foreach (InventoryItem inventoryItem in inventorySystem.items)
            {
                if (exiesItems.ContainsKey(inventoryItem.item))
                {
                    GameObject item = exiesItems[inventoryItem.item];
                    ItemGameObjectUI itemGameObject = item.GetComponent<ItemGameObjectUI>();
                    if (itemGameObject != null)
                    {
                        itemGameObject.UpdateItem(inventoryItem.amount);
                    }
                    else
                    {
                        Debug.LogWarning("ItemGameObject component not found on instantiated item.");
                    }
                }
                else
                {
                    GameObject item = Instantiate(prefabsItemInventory, perentInventory);
                    ItemGameObjectUI itemGameObject = item.GetComponent<ItemGameObjectUI>();
                    if (itemGameObject != null)
                    {
                        itemGameObject.SetItem(inventoryItem);
                        exiesItems.Add(inventoryItem.item, item);
                        itemGameObject.OnClikItemEvent += ShowDescription;
                    }
                    else
                    {
                        Debug.LogWarning("ItemGameObject component not found on prefabItemInventory.");
                    }
                }
            }
        }
    }

    public void ShowDescription(InventoryItem item)
    {
        desText.text = item.item.description;
        desImage.sprite = item.item.itemIcon;
        Color imageColor = desImage.color;
        imageColor.a = 1f;
        desImage.color = imageColor;
    }

    void initDescription()
    {
        desText.text = "";
        Color imageColor = desImage.color;
        imageColor.a = 0f;
        desImage.color = imageColor;
    }

}
