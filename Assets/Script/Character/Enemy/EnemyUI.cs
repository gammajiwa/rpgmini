using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
public partial class Enemy : Character
{
    [SerializeField]
    private Image hpBar;
    void updateUIBar(float value)
    {
        hpBar.fillAmount = value / atributCharacter.HealthPoint;
    }
}
