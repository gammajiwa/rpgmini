using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
public partial class Enemy : Character
{
    void EnemyNormalAtk()
    {
        rotationPlayer(player.transform.position);
        if (!IsAttacking)
        {
            IsAttacking = true;
            // meleeCollider.SetActive(false);
            intervalAttack = atributCharacter.SpeedAtk;

            if (weaponAtribut.weapon.weaponType == WeaponType.Melee)
            {
                PlayStateAnimation(StatAnimation.Attack_Normal, "1");
            }
            if (weaponAtribut.weapon.weaponType == WeaponType.Range)
            {
                PlayStateAnimation(StatAnimation.Attack_Bow);
                GameObject projectile = ObjectPoolManager.Instance.GetObjectFromPool("EnemyProjectile", transform.position, Quaternion.identity);
                projectile.GetComponent<EnemyProjectile>().SetProjectile(player, atributCharacter.Dexterity, DamageEnum.phscDmg);
            }
        }
    }
}
