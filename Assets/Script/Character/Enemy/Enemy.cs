using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public partial class Enemy : Character
{
    private Transform player, originPosition;
    public NavMeshAgent EnemyAI;
    public GameObject VfxDamage;
    public SystemDropItem systemDropItem;
    private void Awake()
    {

    }
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        originPosition = transform;
    }

    protected override void Move()
    {
        base.Move();
        MoveEnemy();
    }
    protected override void Damageing(DamageEnum type, float Damage)
    {
        base.Damageing(type, Damage);
        PopUpScore.AddPopUp(Damage, transform.position);
    }

    public void TakeDamage(DamageEnum type, float Damage)
    {
        if (state != StateCondition.dead)
        {
            Damageing(type, Damage);
            updateUIBar(atributCharacter.CurrHealthPoint);
            MoveEnemy();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "sword")
        {
            Player _player = player.GetComponent<Player>();
            if (_player.weaponAtribut.weapon.weaponType == WeaponType.Melee)
            {
                TakeDamage(_player.weaponAtribut.weapon.damageType, _player.atributCharacter.Dexterity);
                VfxHit(3);
            }
        }
    }

    void MoveEnemy()
    {
        if (state != StateCondition.dead)
        {
            if (Vector3.Distance(transform.position, player.transform.position) >= atributCharacter.distanceChase)
            {
                if (Vector3.Distance(transform.position, player.transform.position) <= atributCharacter.ApproachableDistance)
                {
                    rotationPlayer(player.transform.position);
                    PlayStateAnimation(StatAnimation.run);
                    EnemyAI.SetDestination(new Vector3(player.position.x, player.position.y, 0));
                }
            }
            else
            {
                EnemyAI.ResetPath();
                EnemyNormalAtk();
            }
        }
        else
        {
            EnemyAI.ResetPath();
            StartCoroutine(dead());
        }
    }

    protected override void SetSpeedCharater()
    {
        base.SetSpeedCharater();
        EnemyAI.speed = atributCharacter.MoveSpeed;
    }

    IEnumerator dead()
    {
        yield return new WaitForSeconds(1);
        systemDropItem.GetDropItem();
        gameObject.SetActive(false);
    }
}
