using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AI;
public partial class Player : Character
{
    public void ShootProjectile()
    {
        if (weaponAtribut.weapon.weaponType == WeaponType.Range)
            if (IsCanAtk)
                if (!IsAttacking)
                {
                    rotationPlayer(ObjectTarget.transform.position);
                    PlayStateAnimation(StatAnimation.Attack_Bow);
                    GameObject projectile = ObjectPoolManager.Instance.GetObjectFromPool("Projectile", transform.position, Quaternion.identity);
                    projectile.GetComponent<PlayerProjectile>().SetProjectile(ObjectTarget, atributCharacter.Dexterity, DamageEnum.phscDmg);
                    IsAttacking = true;
                    intervalAttack = atributCharacter.SpeedAtk;
                }
    }

}
