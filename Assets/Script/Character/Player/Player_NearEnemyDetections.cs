using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AI;
public partial class Player : Character
{
    [SerializeField]
    public Dictionary<GameObject, Enemy> listEnmyInRange = new Dictionary<GameObject, Enemy>();

    public Transform ObjectTarget;
    private void ChekingEnemyInRange(Enemy enemy)
    {
        if (!listEnmyInRange.ContainsKey(enemy.gameObject))
        {
            listEnmyInRange.Add(enemy.gameObject, enemy);
            PickObject();
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        Enemy enemy = other.gameObject.GetComponent<Enemy>();
        if (enemy != null)
        {
            if (listEnmyInRange != null)
            {
                listEnmyInRange.Remove(enemy.gameObject);
                PickObject();
            }
        }
    }

    private GameObject NearObjek()
    {
        GameObject objekNear = null;
        float rangeNear = Mathf.Infinity;

        foreach (KeyValuePair<GameObject, Enemy> objekPair in listEnmyInRange)
        {
            GameObject objek = objekPair.Key; // Mengambil GameObject dari KeyValuePair
            float range = Vector3.Distance(transform.position, objek.transform.position);

            if (range < rangeNear)
            {
                rangeNear = range;
                objekNear = objek;
            }
        }

        return objekNear;
    }

    private void PickObject()
    {
        if (NearObjek() != null)
        {
            ObjectTarget = NearObjek().transform;
            ShootProjectile();
        }
        else
            ObjectTarget = null;
    }
}
