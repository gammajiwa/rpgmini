using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AI;
public partial class Player : Character
{
    Vector3 targetMove;
    public ScriptableObjectCharater armorAtribut;
    public ScriptableObjectCharater[] additionalItemAtribut;
    public NavMeshAgent player;
    public bool IsCanAtk;
    private void Start()
    {
        IsAttacking = false;
        IsCanAtk = true;

    }
    protected override void Move()
    {
        base.Move();
        if (Input.GetMouseButtonDown(1))
        // if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            targetMove = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(targetMove, Vector2.zero);
            if (hit.collider != null)
            {
                Debug.Log(hit.collider.name);
                MovePlayer();
            }
            else
                MovePlayer();
        }
        moveStop();
    }

    private void MovePlayer()
    {
        player.SetDestination(new Vector3(targetMove.x, targetMove.y, 0));
        PlayStateAnimation(StatAnimation.run);
        IsCanAtk = false;
    }

    void moveStop()
    {
        if (statAnimation == StatAnimation.run)
        {
            //Rotation
            rotationPlayer(targetMove);

            float distance = Vector3.Distance(transform.position, new Vector3(targetMove.x, targetMove.y, 0));
            if (distance <= 2)
            {
                PlayStateAnimation(StatAnimation.idle);
                IsCanAtk = true;
            }
        }
    }

    protected override void UpdateFrame()
    {

        if (listEnmyInRange != null)
            PickObject();
        if (IsAttacking)
        {
            UIManager.Instance.EnergyPlayer(intervalAttack, atributCharacter.SpeedAtk);
        }
    }

    protected override void Damageing(DamageEnum type, float Damage)
    {
        base.Damageing(type, Damage);
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        Enemy enemy = other.gameObject.GetComponent<Enemy>();
        if (IsCanAtk)
        {
            if (enemy != null)
            {
                if (weaponAtribut.weapon.weaponType == WeaponType.Melee)
                    if (!IsAttacking)
                    {
                        IsAttacking = true;
                        // meleeCollider.SetActive(false);
                        intervalAttack = atributCharacter.SpeedAtk;
                        // Random

                        int ran = UnityEngine.Random.Range(1, 5);
                        PlayStateAnimation(StatAnimation.Attack_Normal, "" + ran);
                        rotationPlayer(other.gameObject.transform.position);
                    }
                if (weaponAtribut.weapon.weaponType == WeaponType.Range)
                    ChekingEnemyInRange(enemy);
            }
        }
    }

    protected override void SetSpeedCharater()
    {
        base.SetSpeedCharater();
        player.speed = atributCharacter.MoveSpeed;
    }

}


