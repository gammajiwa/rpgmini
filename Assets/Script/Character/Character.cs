using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public ClassAtribut atributCharacter;
    public SPUM_Prefabs _Animation;
    public ScriptableObjectCharater baseAtribut;
    public ScriptableObjectWeapone weaponAtribut;
    [SerializeField]
    protected GameObject meleeCollider;
    protected float intervalAttack, Rotation = 0;
    protected bool IsAttacking;
    protected StatAnimation statAnimation;
    protected StateCondition state;
    private void OnEnable()
    {
        SetAtribut();
    }
    private void Update()
    {
        gameObject.transform.rotation = Quaternion.Euler(0, Rotation, 0);
        if (IsAttacking)
        {
            intervalAttack -= 1 * Time.deltaTime;
            if (intervalAttack <= 0)
            {
                // if (weaponAtribut.weapon.weaponType == WeaponType.Melee)
                //     
                IsAttacking = false;
            }
        }
        UpdateFrame();
        Move();
    }
    protected virtual void Move() { }
    protected virtual void UpdateFrame() { }
    protected virtual void Attacking() { }
    protected virtual void Damageing(DamageEnum type, float Damage)
    {
        atributCharacter.CurrHealthPoint -= Damage;
        switch ((int)type)
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
        }
        if (atributCharacter.CurrHealthPoint <= 0)
        {
            CharacterDie();
        }
        else { PlayStateAnimation(StatAnimation.Damage); }
    }
    protected virtual void CharacterDie()
    {
        // gameObject.SetActive(false);
        state = StateCondition.dead;
        PlayStateAnimation(StatAnimation.Death);
    }

    protected virtual void SetAtribut()
    {
        atributCharacter.ResetAtribut();
        atributCharacter.AddAtribut(baseAtribut.atribut);
        atributCharacter.AddAtribut(weaponAtribut.weapon.atribut);
        SetAtkRange();
        SetSpeedCharater();
    }

    private void SetAtkRange()
    {
        // if (weaponAtribut.weapon.weaponType == WeaponType.Melee)
        meleeCollider.SetActive(true);
        meleeCollider.transform.localScale = new Vector3(atributCharacter.RankAtk, atributCharacter.RankAtk, atributCharacter.RankAtk);
    }

    public void PlayStateAnimation(StatAnimation state, string value = "")
    {
        statAnimation = state;
        _Animation.PlayAnimation(state + value);
    }

    protected void rotationPlayer(Vector3 target)
    {
        if (transform.position.x < target.x)
            Rotation = 180;
        else if (transform.position.x > target.x)
            Rotation = 0;
    }

    protected virtual void SetSpeedCharater() { }

    public void VfxHit(int type)
    {
        Vector3 Loc = new Vector3(Random.Range(transform.position.x - 2, transform.position.x + 2), transform.position.y, 0);
        GameObject obj = ObjectPoolManager.Instance.GetObjectFromPool("hitvfx", Loc, Quaternion.identity);
        obj.GetComponent<Animator>().Play("hit" + type);
    }
}
