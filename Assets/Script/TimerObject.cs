using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerObject : MonoBehaviour
{
    private float _timer;
    private void Awake()
    {
        SetTimer(2);
    }

    private void Update()
    {
        _timer -= 1 * Time.deltaTime;
        if (_timer <= 0)
            gameObject.SetActive(false);
    }

    public void SetTimer(float value)
    {
        _timer = value;
    }
}
