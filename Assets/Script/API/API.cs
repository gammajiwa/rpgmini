using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using Newtonsoft.Json;

public class API : MonoBehaviour
{
    public static API Instance;
    public PlayerData playerData;
    public Currency currency;
    public SaveData saveData;
    public string Version;

    private void Awake()
    {
        Instance = this;
    }

    void OnError(PlayFabError error)
    {
        Debug.Log(error.GenerateErrorReport());
    }
    public void LoginGuest()
    {
        var request = new LoginWithCustomIDRequest
        {
            CustomId = SystemInfo.deviceUniqueIdentifier,
            CreateAccount = true,
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
            {
                GetPlayerProfile = true,
            }
        };
        PlayFabClientAPI.LoginWithCustomID(request, OnLogin, OnError);
    }
    void OnLogin(LoginResult result)
    {
        Debug.Log("login sukses");
        string name = null;
        if (result.InfoResultPayload.PlayerProfile.DisplayName != null)
        {
            name = result.InfoResultPayload.PlayerProfile.DisplayName;
            Debug.Log(name);
            playerData.Name = name;
            GetDataVersion();
            GetSaveData();
        }
        else
        {
            UIAPI.Instance.TabSetname.SetActive(true);
        }
    }

    public void PostUserName()
    {
        var request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = UIAPI.Instance.inputName.text
        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnSetName, OnError);
    }
    void OnSetName(UpdateUserTitleDisplayNameResult result)
    {
        UIAPI.Instance.TabSetname.SetActive(false);
    }

    void GetDataVersion()
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest(), OnUserVersion, OnError);
    }
    void OnUserVersion(GetUserDataResult result)
    {
        if (result.Data != null && result.Data.ContainsKey("Version"))
            Version = result.Data["Version"].Value;
    }

    void GetSaveData()
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest(), OnSaveData, OnError);
    }
    void OnSaveData(GetUserDataResult result)
    {
        if (result.Data != null && result.Data.ContainsKey("Save"))
            saveData = JsonConvert.DeserializeObject<SaveData>(result.Data["Save"].Value);
    }

}
