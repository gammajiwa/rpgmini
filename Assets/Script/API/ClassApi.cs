
[System.Serializable]
public class PlayerData
{
    public string Name;
}
[System.Serializable]
public class SaveData
{
    public int Lv;
    public bool Tutorial;
}

[System.Serializable]
public class Currency
{
    public int Coin;
    public int Diamond;
}

